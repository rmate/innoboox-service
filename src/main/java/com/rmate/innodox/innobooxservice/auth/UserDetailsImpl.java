package com.rmate.innodox.innobooxservice.auth;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserDetailsImpl implements UserDetails {
	
    private String username;
    private String email;
    private String password;
    private Collection<SimpleGrantedAuthority> authorities;
    private static final long serialVersionUID = 1L;

    public UserDetailsImpl() {    	
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public boolean isAccountNonExpired() {
        // not applicable here
        return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// not applicable here
        return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// not applicable here
        return true;
	}

	@Override
	public boolean isEnabled() {
		// not applicable here
        return true;
	}
	
    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthorities(Collection<SimpleGrantedAuthority> authorities) {
        this.authorities = authorities;
    }
}
