package com.rmate.innodox.innobooxservice.auth;

public class SecurityConstants {
	
    public static final String SECRET = "SecretKeyToGenJWTs";
    private static final long TEN_DAYS = 864_000_000;
    public static final long EXPIRATION_TIME = TEN_DAYS;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
//    public static final String SIGN_UP_URL = "/user/sign-up";
}
