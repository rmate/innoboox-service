package com.rmate.innodox.innobooxservice.auth;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.rmate.innodox.innobooxservice.auth.SecurityConstants.EXPIRATION_TIME;
import static com.rmate.innodox.innobooxservice.auth.SecurityConstants.HEADER_STRING;
import static com.rmate.innodox.innobooxservice.auth.SecurityConstants.SECRET;
import static com.rmate.innodox.innobooxservice.auth.SecurityConstants.TOKEN_PREFIX;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rmate.innodox.innobooxservice.model.ApplicationUser;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter  {
	
	private AuthenticationManager authenticationManager;
	private final Logger LOG = org.slf4j.LoggerFactory.getLogger(this.getClass());

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(
    		HttpServletRequest request, HttpServletResponse response) 
    				throws AuthenticationException {
        try {
            ApplicationUser user = new ObjectMapper()
                    .readValue(request.getInputStream(), ApplicationUser.class);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            user.getUsername(),
                            user.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(
    		HttpServletRequest request, HttpServletResponse response, 
    		FilterChain chain, Authentication auth) 
    				throws IOException, ServletException {

        String token = JWT.create()
                .withSubject(((User) auth.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(SECRET.getBytes()));
        
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(token);
    }
}
