package com.rmate.innodox.innobooxservice.auth;

import static java.util.Collections.emptyList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.rmate.innodox.innobooxservice.model.ApplicationUser;
import com.rmate.innodox.innobooxservice.model.EmailNotFoundException;
import com.rmate.innodox.innobooxservice.service.ApplicationUserService;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
	
    @Autowired
    ApplicationUserService applicationUserService;
    
    @Autowired
    Converter<ApplicationUser, UserDetails> userToUserDetails;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		ApplicationUser applicationUser = applicationUserService.findByEmail(email);
        if (applicationUser == null) {
            throw new EmailNotFoundException(email);
        }
        return new User(
        		applicationUser.getUsername(), 
        		applicationUser.getPassword(), 
        		emptyList());
	}
}
