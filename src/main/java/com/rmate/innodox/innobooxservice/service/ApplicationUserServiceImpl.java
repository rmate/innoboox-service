package com.rmate.innodox.innobooxservice.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.rmate.innodox.innobooxservice.model.EmailNotFoundException;
import com.rmate.innodox.innobooxservice.model.ApplicationUser;
import com.rmate.innodox.innobooxservice.repository.ApplicationUserRepository;

@Service("userService")
public class ApplicationUserServiceImpl implements ApplicationUserService {
	
	private final Logger LOG = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
    @Autowired
    private ApplicationUserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Override
    public void addDefaultUsers() {
    	List<ApplicationUser> existingUsers = findAll();
    	if (existingUsers.size() == 0) {
    		ApplicationUser defaultUser1 = new ApplicationUser();
    		defaultUser1.setUsername("user1");
    		defaultUser1.setEmail("user1@email.hu");
    		defaultUser1.setPassword("user1");
    		ApplicationUser createdDefaultUser1 =createUser(defaultUser1);
    		
    		if (LOG.isInfoEnabled()) {
    			LOG.info("Default user created: " + createdDefaultUser1);
    		}    		
    		
    		ApplicationUser defaultUser2 = new ApplicationUser();
    		defaultUser2.setUsername("user2");
    		defaultUser2.setEmail("user2@email.hu");
    		defaultUser2.setPassword("user2");
    		ApplicationUser createdDefaultUser2 = createUser(defaultUser2); 
    		
    		if (LOG.isInfoEnabled()) {
    			LOG.info("Default user created: " + createdDefaultUser2);
    		}
    	}
    }
    
    @Override
    public ApplicationUser createUser(ApplicationUser user) {
    	user.setCreateDate(new Date());
    	user.setPassword(passwordEncoder.encode(user.getPassword()));

    	return userRepository.save(user);
    }

	@Override
	public ApplicationUser saveUser(ApplicationUser user) {
		return userRepository.save(user);
	}

	@Override
	public List<ApplicationUser> findAll() {
		return userRepository.findAll();
	}
	
	@Override
	public ApplicationUser findByUsername(String username) {
        List<ApplicationUser> users = userRepository.findByUsername(username);

        if (users.size() == 0) {
            throw new UsernameNotFoundException(username);
        }

        return users.get(0);
	}

	@Override
	public ApplicationUser findByEmail(String email) {
        List<ApplicationUser> users = userRepository.findByEmail(email);

        if (users.size() == 0) {
            throw new EmailNotFoundException(email);
        }

        return users.get(0);
	}
	
	@Override
	public ApplicationUser getLoggedInUser() {
		Object username = (String) SecurityContextHolder
				.getContext()
				.getAuthentication()
				.getPrincipal();
		
		if (username == null) {
			throw new UsernameNotFoundException((String) username);
		}

		ApplicationUser user = findByUsername((String) username);		

		if (user == null) {
			throw new UsernameNotFoundException((String) username);
		}
		
		return user;
	}
}
