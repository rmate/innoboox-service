package com.rmate.innodox.innobooxservice.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rmate.innodox.innobooxservice.model.ApplicationUser;
import com.rmate.innodox.innobooxservice.model.Book;
import com.rmate.innodox.innobooxservice.model.BookNotFoundException;
import com.rmate.innodox.innobooxservice.model.NoMoreBookItemToBorrowException;
import com.rmate.innodox.innobooxservice.repository.BookRepository;
import com.rmate.innodox.innobooxservice.util.BookCategory;

@Service("bookService")
public class BookServiceImpl implements BookService {
	
	private final Logger LOG = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private ApplicationUserService applicationUserService;

	@Override
	public void addDefaultBooks() {
    	List<Book> existingBooks = findAll();
    	
    	if (existingBooks.size() == 0) {
    		Book defaultBook1 = new Book();
    		defaultBook1.setTitle("A kastély");
    		defaultBook1.setAuthors("Franz Kafka");
    		defaultBook1.setPublisher("Magvető");
    		defaultBook1.setCategory(BookCategory.FICTION.getId());
    		defaultBook1.setForeword("Előszó A kastély című regényhez.");
    		defaultBook1.setContent("A kastély című regény tartalma.");
    		defaultBook1.setNumberOfItems(3);    		
    		Book createdDefaultBook1 = createBook(defaultBook1);
    		
       		if (LOG.isInfoEnabled()) {
    			LOG.info("Default book created: " + createdDefaultBook1);
    		} 
    		
    		Book defaultBook2 = new Book();
    		defaultBook2.setTitle("Marsbéli krónikák");
    		defaultBook2.setAuthors("Ray Bradbury");
    		defaultBook2.setPublisher("Európa");
    		defaultBook2.setCategory(BookCategory.SCIENCE_FICTION.getId());
    		defaultBook2.setForeword("Előszó a Marsbéli krónikák című kötethez.");
    		defaultBook2.setContent("A Marsbéli krónikák tartalma.");
    		defaultBook2.setNumberOfItems(1);    		
    		Book createdDefaultBook2 = createBook(defaultBook2);
    		
       		if (LOG.isInfoEnabled()) {
    			LOG.info("Default book created: " + createdDefaultBook2);
    		}
    		
    		Book defaultBook3 = new Book();
    		defaultBook3.setTitle("A nagy terv");
    		defaultBook3.setAuthors("Stephen Hawking, Leonard Mlodinow");
    		defaultBook3.setPublisher("Akkord");
    		defaultBook3.setCategory(BookCategory.SCIENCE.getId());
    		defaultBook3.setForeword("Előszó A nagy terv című kötethez.");
    		defaultBook3.setContent("A nagy terv tartalma.");
    		defaultBook3.setNumberOfItems(2);    		
    		Book createdDefaultBook3 =  createBook(defaultBook3);
    		
       		if (LOG.isInfoEnabled()) {
    			LOG.info("Default book created: " + createdDefaultBook3);
    		}
    	}
	}

	@Override
	public Book saveBook(Book book) {
		return bookRepository.save(book);
	}

	@Override
	public Book createBook(Book book) {
		book.setCreateDate(new Date());
		return bookRepository.save(book);		
	}

	@Override
	public List<Book> findAll() {
		return bookRepository.findAll();
	}
	
	@Override
	public Book findById(long id) {
		Optional<Book> optionalBook = bookRepository.findById(id);
		
		return optionalBook.get();
	}
	
	@Override
	public List<Book> findBooksBorrowedByLoggedInUser(ApplicationUser user) {
		
		Set<Long> bookIds = user.getBooksBorrowed();
		List<Book> books = new ArrayList<>(bookIds.size());
		bookIds.stream().forEach(id -> {
			Book book = bookRepository.findById(id)
							.orElse(null);
			books.add(book);
		});
		
		return books;
	}
	
	@Override
	public Book borrowBook(long bookId, ApplicationUser user) {
		Book book = findById(bookId);
		
		if (book == null) {
			throw new BookNotFoundException(bookId);
		}
		
		int numberOfItems = book.getNumberOfItems();
		
		if (numberOfItems == 0) {
			throw new NoMoreBookItemToBorrowException(book.getTitle());
		}
		
		user.borrowBook(book);
		applicationUserService.saveUser(user);
		
		book.setNumberOfItems(--numberOfItems);
		return saveBook(book);
	}
	
	@Override
	public Book returnBook(long bookId, ApplicationUser user) {
		Book book = findById(bookId);
		if (book == null) {
			throw new BookNotFoundException(bookId);
		}
		
		user.returnBook(bookId);
		applicationUserService.saveUser(user);
		
		int numberOfItems = book.getNumberOfItems();
		
		book.setNumberOfItems(++numberOfItems);
		
		return saveBook(book);
	}
	
	@Override
	public boolean bookExists(Book newBook) {
		List<Book> books = bookRepository.findAll();
		for (Book existingBook : books) {
			if (booksAreIdentical(existingBook, newBook)) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean booksAreIdentical(Book existingBook, Book newBook) {

		return titlesAreTheSame(existingBook, newBook) && 
			   authorsAreTheSame(existingBook, newBook);
	}
	
	private boolean titlesAreTheSame(Book existingBook, Book newBook) {
		String existingTitle = getCleanString(existingBook.getTitle());
		String newTitle = getCleanString(newBook.getTitle());
		
		return existingTitle.equals(newTitle);
	}
	
	private boolean authorsAreTheSame(Book existingBook, Book newBook) {
		List<String> existingAuthors = existingBook.getAuthors();
		List<String> newAuthors = newBook.getAuthors();
		
		if (existingAuthors.size() != newAuthors.size()) {
			return false;
		}
		
		Collections.sort(existingAuthors);
		Collections.sort(newAuthors);
		
		for (int i = 0; i < existingAuthors.size(); i++) {
			String existingAuthor = getCleanString(existingAuthors.get(i));
			String newAuthor = getCleanString(newAuthors.get(i));
			
			if (!existingAuthor.equals(newAuthor)) {
				return false;
			}
		}
		
		return true;
	}
	
	private String getCleanString(String s) {
		return s.toLowerCase().replaceAll("\\s+", "");
	}
}
