package com.rmate.innodox.innobooxservice.service;

import java.util.List;

import com.rmate.innodox.innobooxservice.model.ApplicationUser;

public interface ApplicationUserService {
	void addDefaultUsers();
    ApplicationUser saveUser(ApplicationUser user);
    ApplicationUser createUser(ApplicationUser user);
    ApplicationUser findByUsername(String username);
    ApplicationUser findByEmail(String email);
    ApplicationUser getLoggedInUser();
    List<ApplicationUser> findAll();
}
