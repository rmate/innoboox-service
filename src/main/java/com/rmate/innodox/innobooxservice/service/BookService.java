package com.rmate.innodox.innobooxservice.service;

import java.util.List;

import com.rmate.innodox.innobooxservice.model.ApplicationUser;
import com.rmate.innodox.innobooxservice.model.Book;

public interface BookService {
	void addDefaultBooks();
    Book saveBook(Book book);
    Book createBook(Book book);
    Book findById(long id);
    List<Book>findBooksBorrowedByLoggedInUser(ApplicationUser user);
    Book borrowBook(long bookId, ApplicationUser user);
    Book returnBook(long bookId, ApplicationUser user);
    List<Book> findAll();
    boolean bookExists(Book book);
}
