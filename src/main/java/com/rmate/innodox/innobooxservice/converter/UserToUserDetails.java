package com.rmate.innodox.innobooxservice.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.rmate.innodox.innobooxservice.auth.UserDetailsImpl;
import com.rmate.innodox.innobooxservice.model.ApplicationUser;

@Component
public class UserToUserDetails implements Converter<ApplicationUser, UserDetails> {

	@Override
	public UserDetails convert(ApplicationUser user) {
        UserDetailsImpl userDetails = new UserDetailsImpl();

        if (user != null) {
            userDetails.setUsername(user.getUsername());
            userDetails.setPassword(user.getPassword());
        }

        return userDetails;
	}
}
