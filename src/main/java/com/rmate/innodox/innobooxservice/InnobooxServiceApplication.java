package com.rmate.innodox.innobooxservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.rmate.innodox.innobooxservice.service.ApplicationUserServiceImpl;

@SpringBootApplication
public class InnobooxServiceApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(InnobooxServiceApplication.class, args);
	}
}
