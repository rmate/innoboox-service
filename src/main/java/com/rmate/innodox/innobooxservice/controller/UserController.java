package com.rmate.innodox.innobooxservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rmate.innodox.innobooxservice.service.ApplicationUserService;

@RestController
public class UserController {
	
	@Autowired
	ApplicationUserService applicationUserService;
	
	private String getLoggedInUserName() {
		Object username = (String) SecurityContextHolder
				.getContext()
				.getAuthentication()
				.getPrincipal();

		if (username == null) {
			throw new UsernameNotFoundException((String) username);
		}
		
		return (String) username;
	}

	@GetMapping("get-username")
	public String getUsername() {
		return getLoggedInUserName();
	}
}
