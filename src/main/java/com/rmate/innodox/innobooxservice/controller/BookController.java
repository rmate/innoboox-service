package com.rmate.innodox.innobooxservice.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rmate.innodox.innobooxservice.model.ApplicationUser;
import com.rmate.innodox.innobooxservice.model.Book;
import com.rmate.innodox.innobooxservice.model.BookAlreadyExistsException;
import com.rmate.innodox.innobooxservice.model.BookToDisplay;
import com.rmate.innodox.innobooxservice.service.ApplicationUserService;
import com.rmate.innodox.innobooxservice.service.BookService;
import com.rmate.innodox.innobooxservice.util.BookCategory;

@RestController
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@Autowired
	ApplicationUserService applicationUserService;
	
	private ApplicationUser getLoggedInUser() {
		Object username = (String) SecurityContextHolder
				.getContext()
				.getAuthentication()
				.getPrincipal();

		if (username == null) {
			throw new UsernameNotFoundException((String) username);
		}

		ApplicationUser user = applicationUserService.findByUsername((String) username);		

		if (user == null) {
			throw new UsernameNotFoundException((String) username);
		}
		
		return user;
	}
	
	@GetMapping("get-books")
	public List<BookToDisplay> getBooks() {
		List<Book> books = bookService.findAll();
		List<BookToDisplay> booksToDisplay = new ArrayList<>(books.size());
		ApplicationUser user = getLoggedInUser();
		books.stream().forEach(book -> {
			BookToDisplay bookToDisplay = new BookToDisplay(book, user);
			booksToDisplay.add(bookToDisplay);
		});
		
		return booksToDisplay;
	}
	
	@GetMapping("get-books-by-user")
	public List<BookToDisplay> getBooksByUser() {
		ApplicationUser user = getLoggedInUser();
		List<Book> books = bookService.findBooksBorrowedByLoggedInUser(user);
		List<BookToDisplay> booksToDisplay = new ArrayList<>(books.size());
		books.stream().forEach(book -> {
			BookToDisplay bookToDisplay = new BookToDisplay(book, user);
			booksToDisplay.add(bookToDisplay);
		});
		
		return booksToDisplay;
	}
	
	@PutMapping("borrow-book")
	public BookToDisplay borrowBook(@RequestBody @Valid long id) {
		ApplicationUser user = getLoggedInUser();
		Book book = bookService.borrowBook(id, user);
		BookToDisplay bookToDisplay = new BookToDisplay(book, user);
		
		return bookToDisplay;
	}
	
	@PutMapping("return-book")
	public BookToDisplay returnBook(@RequestBody @Valid long id) {
		ApplicationUser user = getLoggedInUser();
		Book book = bookService.returnBook(id, user);
		BookToDisplay bookToDisplay = new BookToDisplay(book, user);
		
		return bookToDisplay;
	}
	
	@GetMapping("get-categories")
	public Map<Integer, String> getCategories() {
		Map<Integer, String> bookCategories = new TreeMap<>();
		for (BookCategory bc : BookCategory.values()) {
			bookCategories.put(bc.getId(), bc.getName());
		}
		
		return bookCategories;
	}

	@PostMapping("add-book")
	public void add(@RequestBody @Valid Book book) {
		if (bookService.bookExists(book)) {
			throw new BookAlreadyExistsException(book.getTitle());
		}
		
		bookService.createBook(book);
	}
}
