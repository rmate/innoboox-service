package com.rmate.innodox.innobooxservice.dataloader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.rmate.innodox.innobooxservice.service.BookServiceImpl;
import com.rmate.innodox.innobooxservice.service.ApplicationUserServiceImpl;

@Component
public class DataLoader implements ApplicationRunner {

    private ApplicationUserServiceImpl userServiceImpl;
    private BookServiceImpl bookServiceImpl;

    @Autowired
    public DataLoader(ApplicationUserServiceImpl userServiceImpl, BookServiceImpl bookServiceImpl) {
        this.userServiceImpl = userServiceImpl;
        this.bookServiceImpl = bookServiceImpl;
    }

    public void run(ApplicationArguments args) {
    	userServiceImpl.addDefaultUsers();
    	bookServiceImpl.addDefaultBooks();
    }
}
