package com.rmate.innodox.innobooxservice.util;

public enum BookCategory {
	
    FICTION(0, "fiction"),
	SHORT_STORY(1, "short story"),
	SCIENCE_FICTION(2, "science fiction"),
	SCIENCE(3, "science"),
	TRAVEL_GUIDE(4, "travel guide");

    private int id;
    private String name;

    BookCategory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }
    
    public String getName() {
    	return name;
    }
}
