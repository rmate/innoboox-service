package com.rmate.innodox.innobooxservice.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "`book`")
public class Book {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "book_id")
    private Long id;
    
    @NotEmpty
    private String title;
    
    @ElementCollection
    @CollectionTable(name="book_authors")
    private List<String> authors = new ArrayList<>();
    
    @NotEmpty
    private String publisher;
    
    @NotNull
    private Integer category;
    
    @Size(max = 200)
    private String foreword;
    
    @Size(max = 5000)
    private String content;
    
    @Max(10)
    @NotNull
    private Integer numberOfItems;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    
    public Book() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setAuthors(String authors) {		
		this.authors = Arrays.asList(authors.split("\\s*,\\s*"));
	}

	public List<String> getAuthors() {
		return authors;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public String getForeword() {
		return foreword;
	}

	public void setForeword(String foreword) {
		this.foreword = foreword;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(Integer numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", "
				+ "title=" + title + ", "
				+ "authors=" + authors + ", "
				+ "publisher=" + publisher + ", "
				+ "category=" + category + ", "
				+ "foreword=" + foreword + ", "
				+ "content=" + content + ", "
				+ "numberOfItems=" + numberOfItems + ", "
				+ "createDate=" + createDate + "]";
	}	
}
