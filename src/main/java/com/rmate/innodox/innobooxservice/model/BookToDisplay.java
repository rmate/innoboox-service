package com.rmate.innodox.innobooxservice.model;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.rmate.innodox.innobooxservice.service.ApplicationUserService;
import com.rmate.innodox.innobooxservice.util.BookCategory;

public class BookToDisplay {
	
	@Autowired
	private ApplicationUserService applicationUserService;
	
	private final Logger LOG = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	private long id;
	private String title;
	private String authors;
	private String publisher;
	private String category;
	private String foreword;
	private String content;
	private int numberOfItems;
	private boolean isBorrowable;
	
	public BookToDisplay(Book book, ApplicationUser user) {
		setId(book.getId());
		setTitle(book.getTitle());
		setAuthors(String.join(", ", book.getAuthors()));
		setPublisher(book.getPublisher());
		for (BookCategory bc : BookCategory.values()) {
			if (bc.getId() == book.getCategory()) {
				setCategory(bc.getName());
				break;
			}
		}
		setForeword(book.getForeword());
		setContent(book.getContent());
		setNumberOfItems(book.getNumberOfItems());
		setBorrowable(book.getId(), user);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getForeword() {
		return foreword;
	}

	public void setForeword(String foreword) {
		this.foreword = foreword;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(int numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public boolean isBorrowable() {
		return isBorrowable;
	}

	public void setBorrowable(long bookId, ApplicationUser user) {
		this.isBorrowable = numberOfItems > 0 && 
							!user.getBooksBorrowed().contains(bookId);
	}
}
