package com.rmate.innodox.innobooxservice.model;

public class NoMoreBookItemToBorrowException extends RuntimeException {
	
    private static final long serialVersionUID = 1L;
	
	public NoMoreBookItemToBorrowException(String title) {
		super("No more item of the following book: " + title);
	}
}
