package com.rmate.innodox.innobooxservice.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "`application_user`")
public class ApplicationUser {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long id;
    
    @Column(unique = true)
    @Size(min = 5)
    @NotEmpty
    private String username;
    
    @Size(min = 5)
    @NotEmpty
    private String password;
    
    @Email
    @NotEmpty
    @Column(unique = true)
    private String email;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    
    @ElementCollection
    @CollectionTable(name="borrowed_books_of_application_users")
    private Set<Long> booksBorrowed = new HashSet<>();
    
    public ApplicationUser() {    	
    }
    
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
    public Set<Long> getBooksBorrowed() {
		return booksBorrowed;
	}
    
    public void borrowBook(Book book) {
    	if (!booksBorrowed.add(book.getId())) {
    		throw new BookAlreadyBorrowedException(book.getTitle());
    	}
    }
    
    public void returnBook(Long returningBookId) {
    	for (Iterator<Long> iterator = booksBorrowed.iterator(); iterator.hasNext();) {
    	    Long borrowedBookId =  iterator.next();
    	    if (borrowedBookId == returningBookId) {
    	        iterator.remove();
    	    }       
    	}
    }

	@Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", createDate=" + createDate +
                '}';
    }
}
