package com.rmate.innodox.innobooxservice.model;

public class BookAlreadyBorrowedException extends RuntimeException {
	
    private static final long serialVersionUID = 1L;
	
	public BookAlreadyBorrowedException(String title) {
		super("Book already borrowed: " + title);
	}
}
