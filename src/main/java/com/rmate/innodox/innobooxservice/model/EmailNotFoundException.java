package com.rmate.innodox.innobooxservice.model;

public class EmailNotFoundException extends RuntimeException {
    
	private static final long serialVersionUID = 1L;
	
	public EmailNotFoundException(String email) {
		super("User not found with email: " + email);
	}
}
