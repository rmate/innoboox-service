package com.rmate.innodox.innobooxservice.model;

public class BookAlreadyExistsException extends RuntimeException {
	
    private static final long serialVersionUID = 1L;
	
	public BookAlreadyExistsException(String title) {
		super("Book already exists: " + title);
	}
}
