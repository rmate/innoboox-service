package com.rmate.innodox.innobooxservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rmate.innodox.innobooxservice.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

}
