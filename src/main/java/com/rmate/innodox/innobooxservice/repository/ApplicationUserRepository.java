package com.rmate.innodox.innobooxservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rmate.innodox.innobooxservice.model.ApplicationUser;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
	List<ApplicationUser> findByUsername(String username);
	List<ApplicationUser> findByEmail(String email);
}
